FROM bash:4.4.12

ADD ./env2file.sh /usr/local/bin/env2file.sh
ADD ./env2file_test.sh /usr/local/test/env2file_test.sh

RUN ln -s /usr/local/bin/bash /bin/bash
RUN ENV2FILEBIN=/usr/local/bin/env2file.sh /usr/local/test/env2file_test.sh

CMD env2file.sh