#!/bin/bash

OLDIFS=$IFS
IFS=$'\n'
for e2fvar in $(env | grep '^E2F_*');do
  if [[ "$e2fvar" =~ E2F.*PATH.* ]];then
    pathname="${e2fvar%=*}"
    pathval="${e2fvar##*=}"
    varname="${pathname%_*}"
    contentname="${varname}_CONTENT"
    contentval="${!contentname}"

    #echo -e "varname = $varname\npathname = $pathname\npathval = $pathval\ncontentname = $contentname\ncontentval = $contentval"

    mkdir -p $(dirname "$pathval")
    echo -e "$contentval" > "$pathval"
  fi
done
IFS=$OLDIFS
