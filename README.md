env2file
=========

This docker image converts environment variables to files.

The first purpose of this image is to be used as **sidekick** in **rancher** cattle orchestrator to configure dynamically the services with env var.

## How to use

Start the container with env variable composed like : `E2F_[THEVARNAME]_PATH` for the path to the file, and `E2F_[THEVARNAME]_CONTENT` for the content of the file.

- The path must be absolute
- The content may contain line returns, or `\n`


Simple json file example :
```bash
docker run \
  -e E2F_ES_PATH=/usr/local/config/config.json \
  -e E2F_ES_CONTENT='{"user": { "name": "dulute" } }' \
  registry.gitlab.com/recalbox/ops/env2file:latest
```

The file `/usr/local/config/config.json` is created and contains the text : `{"user": { "name": "dulute" } }`

Multiline yaml example:
```bash
docker run \
  -e E2F_ES_PATH=/usr/local/config/config.yml \
  -e E2F_ES_CONTENT='logstash-indexer:
  metadata:
    logstash:
      inputs: |
        redis {
          host => "redis.rancher.internal"
          port => "6379"
          data_type => "list"
          key => "logstash"
        }
      filters: |
        ${indexer_filters}
      outputs: |
        ${indexer_outputs}
' \
  registry.gitlab.com/recalbox/ops/env2file:latest
```

Variable interpolation example (see the **double quotes**):
```bash
docker run \
  -e E2F_ES_PATH=/usr/local/config/config.yml \
  -e E2F_ES_CONTENT="logstash-indexer:
  metadata:
    logstash:
      filters: |
        ${MY_CURRENT_ENV_VAR}
      outputs: |
        ${MY_CURRENT_ENV_VAR2}
" \
  registry.gitlab.com/recalbox/ops/env2file:latest
```

`MY_CURRENT_ENV_VAR` and `MY_CURRENT_ENV_VAR2` will be replaced by your current environment variables.


## How to build

```bash
docker build -t env2file .
```

## How to contribute

Issues and Merge Requests are open.

Please test the new features with env2file_test.sh.