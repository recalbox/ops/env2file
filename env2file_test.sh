#!/bin/bash

export TEST_DIR=/tmp/env2filetest
env2file=${ENV2FILEBIN:-./env2file.sh}
totaltest=0
function assertThat {
    totaltest=$((totaltest+1))
    case $2 in
    contains)
      if test "$(echo -e "$3")" != "$(cat $1)"; then
        echo -e "\e[31m❌\e[39m Test failed:"
        echo -e "Expected string : \n$3\ndo not equals to $1 content: \n$(cat $1)"
        exit 1
      fi
      ;;
    containsNumberOfLine)
      if test $3 -ne $(cat $1 | wc -l); then
        echo -e "\e[31m❌\e[39m Test failed:"
        echo -e "Expected number of lines : \n$3\ndo not equals to $1 number of lines: \n$(cat $1 | wc -l)"
        exit 1
      fi
      ;;
    *)
      echo "Assertion unknown" && echo -e "\n\n\e[31m❌\e[39m Tests failed"; exit 1
      ;;
    esac
}
function unsetAll {
    unset $(env | grep "E2F.*" | cut -d= -f1)
}

echo "Starting tests"

###################################################################################
# whenFile1PathAndContentAreAvailableThenCreateTheFileWithContent
unsetAll
export E2F_FILE1_PATH="$TEST_DIR/env2file1.txt"
export E2F_FILE1_CONTENT="wtf tdd in bash"
eval $env2file

assertThat "$E2F_FILE1_PATH" contains "$E2F_FILE1_CONTENT"


###################################################################################
# whenFile1PathAndContentAreAvailableThenCreateTheFileWithContentAndDirectory
unsetAll
rm -rf "$TEST_DIR/env2file/unexistingDir"

export E2F_FILE1_PATH="$TEST_DIR/env2file/unexistingDir/file.txt"
export E2F_FILE1_CONTENT="yolo"
eval $env2file
assertThat "$E2F_FILE1_PATH" contains "$E2F_FILE1_CONTENT"


###################################################################################
# whenFile2PathAndContentAreAvailableThenCreateTheFileWithContentAndDirectory
unsetAll
rm -rf "$TEST_DIR/env2file2/"

export E2F_FILE2_PATH="$TEST_DIR/env2file2/file2.txt"
export E2F_FILE2_CONTENT="thesecondfile"
eval $env2file
assertThat "$E2F_FILE2_PATH" contains "$E2F_FILE2_CONTENT"


###################################################################################
# whenPassingVariableWithMultipleUnderscoreThenCreateTheFileWithContentAndDirectory
unsetAll
export E2F_FILE1_BIS_PATH="$TEST_DIR/env2filebis.txt"
export E2F_FILE1_BIS_CONTENT="wtf tdd in bash"
eval $env2file
assertThat "$E2F_FILE1_BIS_PATH" contains "$E2F_FILE1_BIS_CONTENT"


###################################################################################
# whenPassingVariableWithEqualsCharThenCreateTheFileWithContentAndDirectory
unsetAll
export E2F_FILE1_EQUALS_PATH="$TEST_DIR/env2fileeq.txt"
export E2F_FILE1_EQUALS_CONTENT="wtf=tdd in bash"
eval $env2file
assertThat "$E2F_FILE1_EQUALS_PATH" contains "$E2F_FILE1_EQUALS_CONTENT"

###################################################################################
# whenPassingMultipleVariableThenCreateTheFileWithContentAndDirectory
unsetAll
export E2F_FILE2_PATH="$TEST_DIR/env2file2/file2.txt"
export E2F_FILE2_CONTENT="thesecondfile"
export E2F_FILE1_BIS_PATH="$TEST_DIR/env2filebis.txt"
export E2F_FILE1_BIS_CONTENT="wtf tdd in bash"
export E2F_FILE1_EQUALS_PATH="$TEST_DIR/env2fileeq.txt"
export E2F_FILE1_EQUALS_CONTENT="wtf=tdd in bash"
eval $env2file
assertThat "$E2F_FILE2_PATH" contains "$E2F_FILE2_CONTENT"
assertThat "$E2F_FILE1_BIS_PATH" contains "$E2F_FILE1_BIS_CONTENT"
assertThat "$E2F_FILE1_EQUALS_PATH" contains "$E2F_FILE1_EQUALS_CONTENT"

###################################################################################
# whenPassingMultipleLineWithEscapedLRThenCreateTheFileWithContentAndDirectory
unsetAll
export E2F_FILE_PATH="$TEST_DIR/fileWithLineReturn.txt"
export E2F_FILE_CONTENT="oneline\nandanother"
eval $env2file
assertThat "$E2F_FILE_PATH" contains "$E2F_FILE_CONTENT"
assertThat "$E2F_FILE_PATH" containsNumberOfLine 2


###################################################################################
# whenPassingMultipleLineThenCreateTheFileWithContentAndDirectory
unsetAll
export E2F_FILE_PATH="$TEST_DIR/fileWithLineReturn2.txt"
export E2F_FILE_CONTENT="oneline
andanother
still another"
eval $env2file
assertThat "$E2F_FILE_PATH" contains "$E2F_FILE_CONTENT"
assertThat "$E2F_FILE_PATH" containsNumberOfLine 3


###################################################################################
# whenPassingVariableThenInterpolate
unsetAll
name="dulutre"
export E2F_FILE_PATH="$TEST_DIR/fileWithLineReturn2.txt"
export E2F_FILE_CONTENT="user:
  name: ${NAME}"
eval $env2file
assertThat "$E2F_FILE_PATH" contains "$E2F_FILE_CONTENT"


###################################################################################
# whenPassingVariableWithoutDoubleQuotesThenKeptInTheFile
unsetAll
name="dulutre"
export E2F_FILE_PATH="$TEST_DIR/fileWithLineReturn2.txt"
export E2F_FILE_CONTENT='user:
  name: ${NAME}'
eval $env2file
assertThat "$E2F_FILE_PATH" contains 'user:
  name: ${NAME}'


###################################################################################
###################################################################################
echo -e "\e[32m✔\e[39m All $totaltest tests passed"
echo -e "Cleaning test files"
rm -rf "$TEST_DIR"
